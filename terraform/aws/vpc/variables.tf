variable "azs_count" {
  type = number
}
variable "use_private_subnets" {
  type = bool
}
variable "vpc_cidr_block" {
  type = string
}
variable "name" {
  type = string
}
variable "use_es" {
  type    = bool
  default = false
}
variable "es_name" {
  default = ""
}
variable "use_db" {
  type    = bool
  default = false
}