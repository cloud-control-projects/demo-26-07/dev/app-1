output "cname_prefix" {
  value = local.beanstalk_prefix
}

output "certificate_arn" {
  value = aws_acm_certificate.certificate_self_signed.arn
}